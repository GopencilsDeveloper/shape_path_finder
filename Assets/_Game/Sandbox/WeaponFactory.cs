﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class WeaponFactory : ScriptableObject
{
    #region Editor Params
    [SerializeField]
    Weapon[] prefabs;

    [SerializeField]
    public int[] weaponsAmount;

    [SerializeField]
    bool recycle;
    #endregion

    #region Params
    List<Weapon>[] pools;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnValidate()
    {
        if (weaponsAmount.Length != prefabs.Length)
        {
            Debug.LogError("weaponsAmount Length must be equal prefabs Length");
        }
    }

    public Weapon Get(int weaponId)
    {
        Weapon instance;
        if (recycle)
        {
            if (pools == null)
            {
                CreatePools();
            }
            List<Weapon> pool = pools[weaponId];
            int lastIndex = pool.Count - 1;
            if (lastIndex >= 0)
            {
                instance = pool[lastIndex];
                instance.gameObject.SetActive(true);
                pool.RemoveAt(lastIndex);
            }
            else
            {
                instance = Instantiate(prefabs[weaponId]);
                instance.WeaponId = weaponId;
            }
        }
        else
        {
            instance = Instantiate(prefabs[weaponId]);
            instance.WeaponId = weaponId;
        }
        return instance;
    }

    void CreatePools()
    {
        pools = new List<Weapon>[prefabs.Length];
        for (int i = 0; i < pools.Length; i++)
        {
            pools[i] = new List<Weapon>();
        }
    }

    public void Reclaim(Weapon weaponToRecycle)
    {
        if (recycle)
        {
            if (pools == null)
            {
                CreatePools();
            }
            pools[weaponToRecycle.WeaponId].Add(weaponToRecycle);
            weaponToRecycle.gameObject.SetActive(false);
        }
        else
        {
            Destroy(weaponToRecycle.gameObject);
        }
    }

    #endregion
}
