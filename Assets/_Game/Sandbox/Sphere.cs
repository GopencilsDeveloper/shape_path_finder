﻿using UnityEngine;

public class Sphere : Weapon
{
    public override void Action()
    {
        print("Sphere");
    }
    private void Awake()
    {
        this.WeaponId = 0;
    }

    private void Reclaim()
    {
        WeaponManager.Instance.weaponFactory.Reclaim(this);
    }
}
