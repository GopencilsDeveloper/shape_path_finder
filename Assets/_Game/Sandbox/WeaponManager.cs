﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WeaponManager : MonoBehaviour
{
    #region Const
    public const float maxPullMagnitude = 80f;
    private const float maxArrowScale = 5f;
    #endregion

    #region Editor Params
    public WeaponFactory weaponFactory;
    public Transform weaponsParent;
    public Launcher launcher;
    #endregion

    #region Params
    private static WeaponManager instance;
    List<Weapon> weapons;
    private Weapon currentWeapon;
    private int currentWeaponId;
    public int[] weaponsAmount;
    public bool isOutOfWeapons = false;
    public int totalWeaponAmount;
    #endregion

    #region Properties
    public static WeaponManager Instance { get => instance; set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        weapons = new List<Weapon>();
        weaponsAmount = new int[weaponFactory.weaponsAmount.Length];
    }

    private void OnEnable()
    {
        instance = this;
        RegisterEvents();
        SelectWeapon(0);
    }

    private void OnDisable()
    {
        UnRegisterEvents();
    }

    public void Initialize()
    {
        currentWeaponId = 0;
        System.Array.Copy(weaponFactory.weaponsAmount, weaponsAmount, weaponFactory.weaponsAmount.Length);
        isOutOfWeapons = false;
        ReclaimAllWeapons();
        UpdateTotalAmount();
        UIManager.Instance.UpdateWeaponAmounts();
    }

    public void UpdateTotalAmount()
    {
        totalWeaponAmount = weaponsAmount.Sum();
        if (totalWeaponAmount == 0)
        {
            isOutOfWeapons = true;
            if (weapons.Count == 0 && !MapManager.Instance.IsCleared())
            {
                GameManager.Instance.LoseGame();
            }
        }
        else
        {
            isOutOfWeapons = false;
        }
    }

    private void RegisterEvents()
    {
        Launcher.OnTouchDownHandler += CreateWeapon;
        Launcher.OnTouchHandler += HoldWeapon;
        Launcher.OnTouchUpHandler += ActionWeapon;
    }

    private void UnRegisterEvents()
    {
        Launcher.OnTouchDownHandler -= CreateWeapon;
        Launcher.OnTouchHandler -= HoldWeapon;
        Launcher.OnTouchUpHandler -= ActionWeapon;
    }

    List<Weapon> bonusWeapons = new List<Weapon>();

    public void CreateWeapon()
    {
        if (GameManager.Instance.currentInGamePhase != InGamePhase.Bonus)
        {
            while (currentWeaponId < weaponsAmount.Length)
            {
                if (weaponsAmount[currentWeaponId] == 0)
                {
                    currentWeaponId++;
                }
                else
                {
                    break;
                }
            }

            if (currentWeaponId >= weaponsAmount.Length)
            {
                currentWeaponId = 0;
            }

            if (weaponsAmount[currentWeaponId] > 0 && launcher.canLaunch)
            {
                currentWeapon = weaponFactory.Get(currentWeaponId);
                currentWeapon.transform.localPosition = launcher.transform.localPosition;
                currentWeapon.transform.SetParent(launcher.transform);
                weapons.Add(currentWeapon);
                weaponsAmount[currentWeaponId]--;
                UIManager.Instance.UpdateWeaponAmounts();
            }
        }
        else    // x5 Bullet in Bonus Phase
        {
            if (launcher.canLaunch)
            {
                currentWeapon = weaponFactory.Get(0);
                currentWeapon.transform.localPosition = launcher.transform.localPosition;
                currentWeapon.transform.SetParent(launcher.transform);
                weapons.Add(currentWeapon);
            }
        }
    }


    public void HoldWeapon()
    {
        currentWeapon.Initialize(new Vector3(0f, -launcher.pullAngle, 0f), launcher.pullMagnitude);
    }

    Vector3 lastDirection;
    float lastPullMag;

    public void ActionWeapon()
    {
        currentWeapon.Action();
        currentWeapon.transform.SetParent(weaponsParent);
        lastDirection = new Vector3(0f, -launcher.pullAngle, 0f);
        lastPullMag = launcher.pullMagnitude;

        if (GameManager.Instance.currentInGamePhase == InGamePhase.Bonus)
        {
            StartCoroutine(C_ColtBullets());
        }
    }

    IEnumerator C_ColtBullets()
    {
        int t = 2;
        while (t > 0)
        {
            yield return new WaitForSeconds(0.5f);
            currentWeapon = weaponFactory.Get(0);
            currentWeapon.transform.localPosition = launcher.transform.localPosition;
            weapons.Add(currentWeapon);
            currentWeapon.Initialize(lastDirection, lastPullMag);
            currentWeapon.transform.SetParent(launcher.transform);
            currentWeapon.transform.localPosition = Vector3.zero;
            currentWeapon.transform.SetParent(weaponsParent);
            currentWeapon.GetComponent<Bullet>().Shoot();
            t--;
        }
    }

    public void SelectWeapon(int weaponId)
    {
        currentWeaponId = weaponId;
    }

    public void ReclaimWeapon(Weapon weaponToRecycle)
    {
        weaponFactory.Reclaim(weaponToRecycle);
    }

    public void ReclaimAllWeapons()
    {
        for (int i = 0; i < weapons.Count; i++)
        {
            weaponFactory.Reclaim(weapons[i]);
        }
        weapons.Clear();
    }

    #endregion
}
