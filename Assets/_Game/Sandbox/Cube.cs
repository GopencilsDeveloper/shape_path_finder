﻿using UnityEngine;

public class Cube : Weapon
{
    public override void Action()
    {
        print("Cube");
    }

    private void Awake()
    {
        this.WeaponId = 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            WeaponManager.Instance.weaponFactory.Reclaim(this);
        }
    }
}
