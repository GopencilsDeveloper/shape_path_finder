using UnityEngine;

public class InstancedIndirect : MonoBehaviour
{
    int instanceCount;
    public Mesh instanceMesh;
    public Material instanceMaterial;

    MapManager mapManager;
    private int cachedInstanceCount = -1;
    private ComputeBuffer positionBuffer;
    private ComputeBuffer argsBuffer;
    private ComputeBuffer colorBuffer;

    private uint[] args = new uint[5] { 0, 0, 0, 0, 0 };

    void OnEnable()
    {
        mapManager = MapManager.Instance;
        argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);

    }

    void Update()
    {
        // Update starting position buffer
        if (cachedInstanceCount != instanceCount) UpdateBuffers();

        // Render
        Graphics.DrawMeshInstancedIndirect(instanceMesh, 0, instanceMaterial, new Bounds(Vector3.one, new Vector3(100.0f, 100.0f, 100.0f)), argsBuffer);
    }

    [NaughtyAttributes.Button]
    public void UpdateBuffers()
    {
        instanceCount = mapManager.transformTiles.Length;
        if (instanceCount < 1) instanceCount = 1;

        // Positions & Colors
        if (positionBuffer != null) positionBuffer.Release();
        if (colorBuffer != null) colorBuffer.Release();

        positionBuffer = new ComputeBuffer(instanceCount, 16);
        colorBuffer = new ComputeBuffer(instanceCount, 4 * 4);

        Vector4[] positions = MapManager.Instance.posTiles;
        Vector4[] colors = MapManager.Instance.colorTiles;

        positionBuffer.SetData(positions);
        colorBuffer.SetData(colors);

        instanceMaterial.SetBuffer("positionBuffer", positionBuffer);
        instanceMaterial.SetBuffer("colorBuffer", colorBuffer);

        // indirect args
        uint numIndices = (instanceMesh != null) ? (uint)instanceMesh.GetIndexCount(0) : 0;
        args[0] = numIndices;
        args[1] = (uint)instanceCount;
        argsBuffer.SetData(args);
        cachedInstanceCount = instanceCount;
    }

    void OnDisable()
    {
        if (positionBuffer != null) positionBuffer.Release();
        positionBuffer = null;

        if (colorBuffer != null) colorBuffer.Release();
        colorBuffer = null;

        if (argsBuffer != null) argsBuffer.Release();
        argsBuffer = null;
    }
}
