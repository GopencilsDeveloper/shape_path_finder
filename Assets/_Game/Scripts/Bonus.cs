﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    #region Editor Params
    public GameObject explosionBonusPS;
    public GameObject[] heads;
    #endregion

    #region Params
    private GameObject currentHead;
    private Animator currentAnimator;
    private Vector3 tempRotation;
    private bool isTidedUp;
    private float mapHeight;
    private int HP = 50;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        tempRotation = transform.localRotation.eulerAngles;
        explosionBonusPS.SetActive(false);
        HP = 50;
    }

    bool cachedExplosion = false;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Bullet") || other.gameObject.CompareTag("Bomb"))
        {
            ScoreManager.Instance.DoScoreFX(0.5f, 10);
            HP--;
            if (HP <= 0 && !cachedExplosion)
            {
                cachedExplosion = true;
                StartCoroutine(C_Explode());
            }
        }
    }

    IEnumerator C_Explode()
    {
        explosionBonusPS.SetActive(true);
        currentHead.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        PullUp();
        yield return new WaitForSeconds(3f);
        explosionBonusPS.SetActive(false);
        HP = 50;
        GameManager.Instance.EndGame();
    }

    [NaughtyAttributes.Button]
    public void RandomHead()
    {
        int rand = UnityEngine.Random.Range(0, heads.Length);

        for (int i = 0; i < heads.Length; i++)
        {
            if (i != rand)
            {
                heads[i].SetActive(false);
            }
        }

        heads[rand].SetActive(true);
        currentHead = heads[rand];
        currentAnimator = currentHead.GetComponent<Animator>();
    }

    Coroutine C_PullDown;

    [NaughtyAttributes.Button]
    public void PullDown()
    {
        transform.localRotation = Quaternion.identity;
        RandomHead();
        C_PullDown = StartCoroutine(C_Pull(new Vector3(0f, 0f, 18.5f), 1f));
        cachedExplosion = false;
    }

    public void PullUp()
    {
        StartCoroutine(C_Pull(new Vector3(0f, 0f, 40f), 1f));
        if (C_PullDown != null)
        {
            StopCoroutine(C_PullDown);
        }
    }

    IEnumerator C_Pull(Vector3 target, float duration = 0f, float durationRotation = 2f)
    {
        var temp = transform.localPosition;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / duration;
            transform.localPosition = Vector3.Lerp(transform.localPosition, target, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }

        float t1 = 0f;

        while (t1 < 1f)
        {
            t1 += Time.deltaTime;
            tempRotation.y = Mathf.Lerp(0f, -30f, Mathf.SmoothStep(0f, 1f, t1));
            transform.rotation = Quaternion.Euler(tempRotation);
            yield return null;
        }

        float t2 = 0f;
        while (t2 >= 0f)
        {
            while (t2 < 1f)
            {
                t2 += Time.deltaTime / durationRotation;
                tempRotation.y = Mathf.Lerp(-30f, 30f, Mathf.SmoothStep(0f, 1f, t2));
                transform.rotation = Quaternion.Euler(tempRotation);
                yield return null;
            }

            t2 = 0f;
            while (t2 < 1f)
            {
                t2 += Time.deltaTime / durationRotation;
                tempRotation.y = Mathf.Lerp(30f, -30f, Mathf.SmoothStep(0f, 1f, t2));
                transform.rotation = Quaternion.Euler(tempRotation);
                yield return null;
            }
            t2 = 0f;
        }
    }

    #endregion
}
