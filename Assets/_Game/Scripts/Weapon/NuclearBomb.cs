﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NuclearBomb : MonoBehaviour
{
    #region Editor Params
    public SimpleFogOfWar.FogOfWarInfluence FOWInfluence;
    public float maxViewDistance = 50f;
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    [NaughtyAttributes.Button]
    public void Explode()
    {
        StartCoroutine(C_Explode());
    }

    IEnumerator C_Explode()
    {
        float t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / 1f;
            FOWInfluence.ViewDistance = Mathf.Lerp(1f, maxViewDistance, Mathf.SmoothStep(0f, 1f, t));
            FOWInfluence.ViewDistanceZ = Mathf.Lerp(1f, maxViewDistance, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
        FOWInfluence.ViewDistance = 0f;
    }
    #endregion
}
