﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    int weaponId;
    bool isShoot;
    #endregion

    #region Properties
    public int WeaponId { get => weaponId; set => weaponId = value; }
    public bool IsShoot { get => isShoot; set => isShoot = value; }
    #endregion

    #region Events
    #endregion

    #region Methods
    public virtual void Initialize(Vector3 direction, float magnitude) { }
    public virtual void Action() { }
    #endregion
}
