﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Weapon
{
    #region Editor Params
    public float currentSpeed;
    public float currentPullRange;
    public float maxSpeed = 20f;
    public float maxPullRange = 4f;
    public SimpleFogOfWar.FogOfWarInfluence FOWInfluence;
    public GameObject[] models;
    public Collider collider;
    #endregion

    #region Params
    private Rigidbody rgbd;
    private float ratioSpeed;
    private float ratioPullRange;
    private Vector3 lastFrameVelocity;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        WeaponId = 0;
        rgbd = GetComponent<Rigidbody>();
        ratioSpeed = maxSpeed / Launcher.maxPullMagnitude;
        ratioPullRange = maxPullRange / Launcher.maxPullMagnitude;
        rgbd.isKinematic = false;
        collider.isTrigger = true;
        FOWInfluence.Suspended = false;
        IsShoot = false;
    }

    private void OnDisable()
    {
        Stop();
    }

    private void Update()
    {
        lastFrameVelocity = rgbd.velocity;
        // if (GameManager.Instance.currentInGamePhase == InGamePhase.Clean && isShoot)
        // {
        //     ScoreManager.Instance.DoScoreFX(1.5f);
        // }
    }

    public override void Initialize(Vector3 direction, float magnitude)
    {
        transform.rotation = Quaternion.Euler(direction);
        currentSpeed = magnitude * ratioSpeed;
        currentSpeed = Mathf.Clamp(currentSpeed, 15f, maxSpeed);
        rgbd.velocity = Vector3.zero;
        Pull(magnitude);
    }

    public void Pull(float pullMagnitude)
    {
        currentPullRange = pullMagnitude * ratioPullRange;
        currentPullRange = Mathf.Clamp(currentPullRange, 0f, maxPullRange);
        transform.localPosition = new Vector3(0f, 0f, -currentPullRange);
    }

    public override void Action()
    {
        Shoot();
        WeaponManager.Instance.UpdateTotalAmount();
    }

    public void Shoot()
    {
        rgbd.velocity = transform.forward * currentSpeed;
        FOWInfluence.Suspended = false;
        Invoke("OffTrigger", 0.2f);
        IsShoot = true;
    }

    void OffTrigger()
    {
        collider.isTrigger = false;
    }

    public void Stop()
    {
        transform.localRotation = Quaternion.identity;
        rgbd.velocity = Vector3.zero;
        rgbd.isKinematic = true;
        currentSpeed = 0f;
        FOWInfluence.Suspended = true;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("Tile"))
        {
            Bounce(other.contacts[0].normal);
        }
        if (GameManager.Instance.currentInGamePhase == InGamePhase.Bonus && other.gameObject.CompareTag("Bonus"))
        {
            ParticleSystem instance = BreakPSPool.Instance.SpawnBonusPS(this.transform.position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.Instance.currentInGamePhase == InGamePhase.Clear && other.CompareTag("Tile"))
        {
            RandomSpawnBreakPS(other.gameObject.GetComponent<Tile>().currentColor, other.transform.localScale);
        }
    }

    private void Bounce(Vector3 collisionNormal)
    {
        var direction = Vector3.Reflect(lastFrameVelocity.normalized, collisionNormal);
        rgbd.velocity = direction * currentSpeed;
    }

    public void RandomSpawnBreakPS(Color color, Vector3 scale)
    {
        int range = (int)(MapManager.Instance.tilesActiveAmount / 500f) + 1;
        if ((int)Random.Range(0, range) != 0)
            return;
        else
        {
            //SpawnBreakPS
            ParticleSystem instance = BreakPSPool.Instance.SpawnBreakPS(this.transform.position);
            BreakPSPool.Instance.SetInterface(ref instance, color, scale);
        }
    }

    #endregion
}
