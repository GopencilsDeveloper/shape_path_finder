﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuriken : Weapon
{
    #region Editor Params
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    public override void Action()
    {
        Pierce();
    }

    public void Pierce()
    {

    }
    #endregion
}
