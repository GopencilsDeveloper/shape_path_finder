﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Launcher : MonoBehaviour
{
    #region  CONST
    public const float maxPullMagnitude = 80f;
    private const float maxArrowScale = 4.5f;
    #endregion

    #region Editor Params
    public GameObject arrow;
    public GameObject pullString;
    #endregion

    #region Params
    private static Launcher instance;
    private Vector3 lastMousePos;
    private Vector3 deltaMousePos;
    public float pullAngle;
    public float pullMagnitude;
    private bool isTouched = false;
    private bool isLaunched = false;

    [HideInInspector]
    public bool canLaunch = true;
    private float arrowScaleRatio = maxArrowScale / maxPullMagnitude;
    private Vector3 tempArrowScale;
    private Vector3 pullStringScale;
    #endregion

    #region Properties
    public static Launcher Instance { get => instance; private set => instance = value; }
    public bool IsLaunched { get => isLaunched; set => isLaunched = value; }
    #endregion

    #region Events
    public static event System.Action OnTouchDownHandler;
    public static event System.Action OnTouchHandler;
    public static event System.Action OnTouchUpHandler;
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
    }

    public void OnEnable()
    {
        Initialize();
    }

    public void Initialize()
    {
        tempArrowScale = arrow.transform.localScale;
        pullStringScale = pullString.transform.localScale;
        isTouched = false;
        isLaunched = false;
        pullMagnitude = 0f;
        pullAngle = 0f;
        lastMousePos = Vector3.zero;
        deltaMousePos = Vector3.zero;
    }

    void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && !WeaponManager.Instance.isOutOfWeapons)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                HandleTouchDown();
            }

            if (isTouched)
            {
                if (Input.GetMouseButton(0))
                {
                    HandleTouch();
                }
                if (Input.GetMouseButtonUp(0))
                {
                    HandleTouchUp();
                }
            }
            arrow.transform.localScale = tempArrowScale;
            pullString.transform.localScale = pullStringScale;
        }
    }

    private void HandleTouchDown()
    {
        // RaycastHit hit;
        // Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // if (Physics.Raycast(ray, out hit, 100f) && hit.collider.CompareTag("Launcher"))
        // {
        isTouched = true;
        lastMousePos = Input.mousePosition;
        arrow.SetActive(true);
        pullString.SetActive(true);
        OnTouchDownHandler();
        // }
    }

    private void HandleTouch()
    {
        deltaMousePos = Input.mousePosition - lastMousePos;
        if (deltaMousePos.y > 0)
        {
            canLaunch = false;
            arrow.SetActive(false);
            pullString.SetActive(false);
            pullMagnitude = 0f;
            transform.localRotation = Quaternion.Euler(Vector3.zero);
            pullAngle = 0f;
        }
        else
        {
            canLaunch = true;
            arrow.SetActive(true);
            pullString.SetActive(true);
            pullMagnitude = Mathf.Abs(Mathf.Clamp(deltaMousePos.y, -maxPullMagnitude, 0f));
            float angle = Vector3.Angle(deltaMousePos, Vector3.down);
            if (deltaMousePos.x < 0f)
                angle = -angle;
            angle = Mathf.Clamp(angle, -60f, 60f);
            transform.localRotation = Quaternion.Euler(0f, -angle, 0f);
            pullAngle = angle;
        }
        tempArrowScale.y = pullMagnitude * arrowScaleRatio;
        pullStringScale.y = pullMagnitude * arrowScaleRatio;
        OnTouchHandler();
    }

    private void HandleTouchUp()
    {
        isTouched = false;
        arrow.SetActive(false);
        pullString.SetActive(false);
        if (canLaunch)
        {
            OnTouchUpHandler();
            if (!isLaunched)
            {
                isLaunched = true;
                FOWManager.Instance.CheckCoverage();
            }
        }
        pullMagnitude = 0f;
    }

    #endregion
}
