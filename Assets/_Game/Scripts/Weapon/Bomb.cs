﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Weapon
{
    #region Editor Params
    public Rigidbody rgbd;
    public float maxThrowForce = 1000f;
    public float maxPullRange = 4f;
    public SimpleFogOfWar.FogOfWarInfluence FOWInfluence;
    public Transform explosionArea;
    public List<GameObject> explosionFXPrefabs;
    public MeshRenderer meshRenderer;
    public ParticleSystem explosionPS;
    #endregion

    #region Params
    private float forceRatio;
    private float throwForce;
    private bool isThrown = false;
    private float currentPullRange;
    private float ratioPullRange;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        this.WeaponId = 1;
        forceRatio = maxThrowForce / Launcher.maxPullMagnitude;
        ratioPullRange = maxPullRange / Launcher.maxPullMagnitude;

        isThrown = false;
        rgbd.isKinematic = false;
        FOWInfluence.ViewDistance = 0f;
        FOWInfluence.ViewDistanceZ = 0f;
        FOWInfluence.Suspended = true;
        meshRenderer.enabled = true;
        explosionPS.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Stop();
    }

    void Stop()
    {
        transform.localRotation = Quaternion.identity;
        throwForce = 0f;
        isThrown = false;
        rgbd.isKinematic = false;
        FOWInfluence.Suspended = true;
        meshRenderer.enabled = false;
        explosionArea.localScale = Vector3.zero;
    }

    public override void Initialize(Vector3 direction, float magnitude)
    {
        transform.localRotation = Quaternion.Euler(direction);
        throwForce = magnitude * forceRatio;
        throwForce = Mathf.Clamp(throwForce, 300f, maxThrowForce);
        Pull(magnitude);
    }

    public void Pull(float pullMagnitude)
    {
        currentPullRange = pullMagnitude * ratioPullRange;
        currentPullRange = Mathf.Clamp(currentPullRange, 0f, maxPullRange);
        transform.localPosition = new Vector3(0f, 0f, -currentPullRange);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground") && isThrown)
        {
            FOWInfluence.Suspended = false;
            rgbd.isKinematic = true;
            Explode();
        }
    }

    public override void Action()
    {
        Throw();
    }

    public void Throw()
    {
        isThrown = true;
        Vector3 force = new Vector3(transform.forward.x * throwForce, 1500f, transform.forward.z * throwForce * 2f);
        rgbd.AddForce(force);
        FOWInfluence.Suspended = true;
    }

    [NaughtyAttributes.Button]
    public void Explode()
    {
        StartCoroutine(C_Explode());
    }

    IEnumerator C_Explode()
    {
        WeaponManager.Instance.UpdateTotalAmount();

        if (GameManager.Instance.currentInGamePhase != InGamePhase.Clean)
        {
            explosionPS.gameObject.SetActive(true);
        }

        // int rand = Random.Range(0, explosionFXPrefabs.Count);
        // Instantiate(explosionFXPrefabs[rand], this.transform.position, Quaternion.identity, this.transform);
        meshRenderer.enabled = false;

        float t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / 0.15f;
            FOWInfluence.ViewDistance = Mathf.Lerp(1f, 10f, Mathf.SmoothStep(0f, 1f, t));
            FOWInfluence.ViewDistanceZ = Mathf.Lerp(1f, 10f, Mathf.SmoothStep(0f, 1f, t));
            explosionArea.localScale = Vector3.Lerp(Vector3.zero, new Vector3(5f, 5f, 1f), Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
        explosionArea.localScale = Vector3.zero;
        yield return new WaitForSeconds(1f);
        WeaponManager.Instance.ReclaimWeapon(this);
    }
    #endregion
}
