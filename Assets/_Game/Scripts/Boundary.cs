﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundary : MonoBehaviour
{
    #region Editor Params
    public Transform[] walls;
    public Transform[] corners;
    #endregion

    #region Params
    CameraManager cameraManager;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Start()
    {
        cameraManager = CameraManager.Instance;
        SetBound();
    }

    void SetBound()
    {
        walls[0].position = new Vector3(cameraManager.Left, 0f, 0f);
        walls[1].position = new Vector3(-cameraManager.Left, 0f, 0f);
        walls[2].position = new Vector3(0f, 0f, cameraManager.Top);
        walls[3].position = new Vector3(0f, 0f, -cameraManager.Top);

        corners[0].position = new Vector3(cameraManager.Left, 0f, -cameraManager.Top);
        corners[1].position = new Vector3(cameraManager.Left, 0f, cameraManager.Top);
        corners[2].position = new Vector3(-cameraManager.Left, 0f, cameraManager.Top);
        corners[3].position = new Vector3(-cameraManager.Left, 0f, -cameraManager.Top);
    }

/*     public void GetBound()
    {
        // create logical plane perpendicular to Y and at (0,0,0):
        var plane = new Plane(Vector3.up, new Vector3(0, 0, 0));
        Ray ray;
        float distance;

        ray = Camera.main.ViewportPointToRay(new Vector3(0, 0, 0)); // bottom left ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 botLeft = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(0, 1, 0)); // top left ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 topLeft = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(0, 1, 0)); // top left ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 topLeft = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(1, 0, 0)); // bottom right ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 botRight = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(1, 1, 0)); // top right ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 topRight = ray.GetPoint(distance);
        }
    } */

    #endregion
}
