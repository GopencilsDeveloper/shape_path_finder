﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakPSPool : MonoBehaviour
{
    #region Editor Params
    public ParticleSystem prefab;
    public ParticleSystem bonusPrefab;
    #endregion

    #region Params
    List<ParticleSystem> poolPSRecycle = new List<ParticleSystem>();
    List<ParticleSystem> poolBonusPSRecycle = new List<ParticleSystem>();
    private static BreakPSPool instance;
    MaterialPropertyBlock propBlock;
    #endregion

    #region Properties
    public static BreakPSPool Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
        propBlock = new MaterialPropertyBlock();
    }

    public ParticleSystem SpawnBreakPS(Vector3 pos)
    {
        ParticleSystem instance;
        int lastIndex = poolPSRecycle.Count - 1;
        if (lastIndex >= 0)
        {
            instance = poolPSRecycle[lastIndex];
            poolPSRecycle.RemoveAt(lastIndex);
            instance.gameObject.SetActive(true);
        }
        else
        {
            instance = Instantiate(prefab);
        }

        instance.transform.position = pos;
        instance.transform.SetParent(this.transform);
        return instance;
    }

    public ParticleSystem SpawnBonusPS(Vector3 pos)
    {
        ParticleSystem instance;
        int lastIndex = poolBonusPSRecycle.Count - 1;
        if (lastIndex >= 0)
        {
            instance = poolBonusPSRecycle[lastIndex];
            poolBonusPSRecycle.RemoveAt(lastIndex);
            instance.gameObject.SetActive(true);
        }
        else
        {
            instance = Instantiate(bonusPrefab);
        }

        instance.transform.position = pos;
        instance.transform.SetParent(this.transform);
        return instance;
    }

    public void SetInterface(ref ParticleSystem ps, Color color, Vector3 scale)
    {
        ParticleSystemRenderer renderer = ps.gameObject.GetComponent<ParticleSystemRenderer>();
        renderer.GetPropertyBlock(propBlock);
        propBlock.SetColor("_Color", color);
        renderer.SetPropertyBlock(propBlock);
        ps.transform.localScale = scale;
    }

    public void Reclaim(ParticleSystem ps)
    {
        poolPSRecycle.Add(ps);
        ps.gameObject.SetActive(false);
    }

    public void ReclaimAll()
    {
        poolPSRecycle.Clear();
        foreach (Transform ps in transform)
        {
            Reclaim(ps.GetComponent<ParticleSystem>());
        }
    }

    public void ReclaimBonus(ParticleSystem ps)
    {
        poolBonusPSRecycle.Add(ps);
        ps.gameObject.SetActive(false);
    }

    public void ReclaimAllBonus()
    {
        poolBonusPSRecycle.Clear();
        foreach (Transform ps in transform)
        {
            ReclaimBonus(ps.GetComponent<ParticleSystem>());
        }
    }

    #endregion
}
