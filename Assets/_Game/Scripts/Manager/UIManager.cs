﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class UIManager : MonoBehaviour
{
    #region Editor Params

    [Header("SCREENS")]
    public GameObject Menu;
    public GameObject InGame;
    public GameObject EndGame;
    public GameObject GameOver;

    [Header("GROUND")]
    public MeshRenderer groundRenderer;
    public Texture[] patterns;
    public Color[] groundColors;

    [Header("COMPONENTS")]
    public Button btnPlay;
    public Button btnRestart;
    public Button btnNext;
    public Button btnBullet;
    public Button btnBomb;
    public GameObject cleanFX;
    public GameObject revealedFX;
    public GameObject clearFX;
    public GameObject bonusFX;
    public Text txtTimer;
    public Text txtBulletAmount;
    public Text txtBombAmount;
    public Image imgPercent;
    public Text txtScore;
    #endregion

    #region Params
    private static UIManager instance;
    #endregion

    #region Properties
    public static UIManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods
    private void Awake()
    {
        instance = this;
    }

    public void RegisterEvents()
    {
        btnPlay.onClick.AddListener(delegate { GameManager.Instance.PlayGame(); });
        btnRestart.onClick.AddListener(delegate { GameManager.Instance.PlayGame(); });
        btnNext.onClick.AddListener(delegate { GameManager.Instance.PlayGame(); });
        btnBullet.onClick.AddListener(delegate { WeaponManager.Instance.SelectWeapon(0); });
        btnBomb.onClick.AddListener(delegate { WeaponManager.Instance.SelectWeapon(1); });
    }
    public void UnregisterEvents()
    {
        btnPlay.onClick.RemoveListener(delegate { GameManager.Instance.PlayGame(); });
        btnRestart.onClick.RemoveListener(delegate { GameManager.Instance.PlayGame(); });
        btnNext.onClick.RemoveListener(delegate { GameManager.Instance.PlayGame(); });
        btnBullet.onClick.RemoveListener(delegate { WeaponManager.Instance.SelectWeapon(0); });
        btnBomb.onClick.RemoveListener(delegate { WeaponManager.Instance.SelectWeapon(1); });
    }

    public void Initialize()
    {
        RandomGround();
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void Update()
    {
        UpdateScore(ScoreManager.Instance.currentScore);
    }

    [NaughtyAttributes.Button]
    public void RandomGround()
    {
        MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
        propBlock.SetColor("_Color", groundColors[Random.Range(0, groundColors.Length)]);
        groundRenderer.SetPropertyBlock(propBlock);
        groundRenderer.material.SetTexture("_MainTex", patterns[Random.Range(0, patterns.Length)]);
    }

    public void UpdateWeaponAmounts()
    {
        txtBulletAmount.text = WeaponManager.Instance.weaponsAmount[0].ToString();
        txtBombAmount.text = WeaponManager.Instance.weaponsAmount[1].ToString();
    }

    public void UpdateCountDown(float seconds)
    {
        txtTimer.text = "";
        TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);
        txtTimer.text = timeSpan.ToString(@"m\:ss");
    }

    public void UpdatePercent(float percent)
    {
        imgPercent.fillAmount = percent;
    }

    public void UpdateScore(int score)
    {
        txtScore.text = score.ToString();
    }

    public void ShowRevealedFX()
    {
        StartCoroutine(C_ShowRevealedFX());
    }

    IEnumerator C_ShowRevealedFX()
    {
        revealedFX.SetActive(true);
        yield return new WaitForSeconds(1f);
        revealedFX.SetActive(false);
    }

    public void ShowCleanFX()
    {
        StartCoroutine(C_ShowCleanFX());
    }

    IEnumerator C_ShowCleanFX()
    {
        cleanFX.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        cleanFX.SetActive(false);
    }

    public void ShowClearFX()
    {
        StartCoroutine(C_ShowClearFX());
    }

    IEnumerator C_ShowClearFX()
    {
        clearFX.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        clearFX.SetActive(false);
    }

    public void ShowBonusFX()
    {
        StartCoroutine(C_ShowBonus());
    }

    IEnumerator C_ShowBonus()
    {
        bonusFX.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        bonusFX.SetActive(false);
    }

    public void ShowMenu()
    {
        Menu.SetActive(true);
        InGame.SetActive(false);
        EndGame.SetActive(false);
        GameOver.SetActive(false);
    }
    public void HideMenu()
    {
        Menu.SetActive(false);
    }
    public void ShowInGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(true);
        EndGame.SetActive(false);
        GameOver.SetActive(false);
    }
    public void HideInGame()
    {
        InGame.SetActive(false);
    }
    public void ShowEndGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(false);
        EndGame.SetActive(true);
        GameOver.SetActive(false);
    }
    public void HideEndGame()
    {
        EndGame.SetActive(false);
    }
    public void ShowGameOver()
    {
        Menu.SetActive(false);
        InGame.SetActive(false);
        EndGame.SetActive(false);
        GameOver.SetActive(true);
    }
    public void HideGameOver()
    {
        GameOver.SetActive(false);
    }

    #endregion
}
