﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOWManager : MonoBehaviour
{
    #region Editor Params
    public SimpleFogOfWar.FogOfWarSystem fogOfWar;
    public FOWLarge fogOfWarLarge;
    public float cleanedPercent = 95f;
    public float checkRate = 10f;
    #endregion

    #region Params
    private static FOWManager instance;
    GameManager gameManager;
    private bool isCleaned;
    private float percent;
    #endregion

    #region Properties
    public static FOWManager Instance { get => instance; set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        instance = this;
        gameManager = GameManager.Instance;
    }

    public void Initialize()
    {
        Generate();
        isCleaned = false;
    }

    public void HideFOW()
    {
        fogOfWar.gameObject.SetActive(false);
        fogOfWarLarge.gameObject.SetActive(false);
    }

    [NaughtyAttributes.Button]
    public void Generate()
    {
        fogOfWar.gameObject.SetActive(true);
        fogOfWarLarge.gameObject.SetActive(true);
        fogOfWar.transform.localPosition = new Vector3(MapManager.Instance.tileParent.position.x, 0f, MapManager.Instance.tileParent.position.z);
        fogOfWar.GenerateFOW(MapManager.Instance.standardTextureSizeX, MapManager.Instance.standardTextureSizeY);
        fogOfWarLarge.Generate();
    }

    public void CheckCoverage()
    {
        StartCoroutine(C_CheckCoverage(checkRate));
    }

    IEnumerator C_CheckCoverage(float checkRate)
    {
        while (true)
        {
            if ((gameManager.currentInGamePhase == InGamePhase.Clean) && Launcher.Instance.IsLaunched && !isCleaned)
            {
                yield return new WaitForSeconds(checkRate);
                Check();
                if (percent > 97f)
                {
                    checkRate = 3f;
                }
                else
                if (percent > 93f)
                {
                    checkRate = 5f;
                }
                else
                if (percent > 60f)
                {
                    checkRate = 7f;
                }
            }
            else
            {
                break;
            }
        }
    }

    [NaughtyAttributes.Button]
    public void Check()
    {
        IsCleaned();
        // Debug.Log("Cleaned: " + percent);
    }

    public bool IsCleaned()
    {
        percent = fogOfWar.CoveredPercent();
        isCleaned = percent >= cleanedPercent;

        if (isCleaned)
        {
            GameManager.Instance.ChangeInGamePhase(InGamePhase.Clear);
            checkRate = 10f;
        }
        return isCleaned;
    }
    #endregion
}
