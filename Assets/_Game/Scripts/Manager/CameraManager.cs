﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    private static CameraManager instance;
    private float left;
    private float top;
    #endregion

    #region Properties
    public static CameraManager Instance { get => instance; set => instance = value; }
    public float Left { get => left; set => left = value; }
    public float Top { get => top; set => top = value; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
        GetEdgesOfScreen();
    }

    void GetEdgesOfScreen()
    {
        float dist = (transform.position - Camera.main.transform.position).y;
        left = -Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f, dist)).x;
        top = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f, dist)).z;
    }

    void GetCameraCornerToWorld()
    {
        // create logical plane perpendicular to Y and at (0,0,0):
        var plane = new Plane(Vector3.up, new Vector3(0, 0, 0));
        Ray ray;
        float distance;
        ray = Camera.main.ViewportPointToRay(new Vector3(0, 0, 0)); // bottom left ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 botLeft = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(0, 1, 0)); // top left ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 topLeft = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(0, 1, 0)); // top left ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 topLeft = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(1, 0, 0)); // bottom right ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 botRight = ray.GetPoint(distance);
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(1, 1, 0)); // top right ray
        if (plane.Raycast(ray, out distance))
        {
            Vector3 topRight = ray.GetPoint(distance);
        }
    }

    #endregion
}
