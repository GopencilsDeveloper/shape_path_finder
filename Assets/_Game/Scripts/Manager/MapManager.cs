﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    #region CONST
    public static string MAPORDER = "MapOrder";
    #endregion

    #region Editor Params
    public InstancedIndirect instancedIndirect;
    public Tile tilePrefab;
    public Transform tileParent;
    public Vector3 tileParentOriginalPos;
    public List<Texture2D> mapTextures;
    public float mapHeight = 2f;
    public float spaceBetweenTiles = 0f;
    public List<Tile> listTiles;
    public Vector4[] posTiles;
    public Vector4[] colorTiles;
    public Transform[] transformTiles;
    public NuclearBomb nuclearBomb;
    public Bonus bonusPrefab;
    public Transform aura;
    #endregion

    #region Params
    private static MapManager instance;
    private int mapCount;
    public int currentMapOrder;
    private int tempIndex = -1;
    List<Tile> poolTilesRecycle = new List<Tile>();
    public int tilesActiveAmount;
    public int totalTilesAmount;
    public float percentClear;
    public bool isTidedUp = false;
    public float standardTextureSizeX = 12;
    public float standardTextureSizeY;
    private bool checkedEnd;
    public Vector3 scale;
    #endregion

    #region Properties
    public static MapManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        mapCount = mapTextures.Count;
        totalTilesAmount = 1;   //Prevent divide 0
    }

    public void Initialize()
    {
        tileParentOriginalPos = tileParent.localPosition;
        tilesActiveAmount = 0;
        isTidedUp = false;
        LoadMapOrder();
        GenerateMap(mapTextures[currentMapOrder]);
        checkedEnd = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            Next();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            Previous();
        }
        if (IsCleared() && !checkedEnd)
        {

            //Bonus: Level %3 ==0 || (cleared&&weaponAmount>0)
            if (currentMapOrder % 3 == 0 || (WeaponManager.Instance.totalWeaponAmount > 0))
            {
                GameManager.Instance.ChangeInGamePhase(InGamePhase.Bonus);
            }
            else //No Bonus
            {
                GameManager.Instance.EndGame();
            }
            checkedEnd = true;
        }
        percentClear = (float)tilesActiveAmount / (float)totalTilesAmount;
        UIManager.Instance.UpdatePercent(1f - percentClear);
    }

    private void Previous()
    {
        tempIndex++;
        tempIndex = tempIndex >= mapCount ? 0 : tempIndex;
        GenerateMap(mapTextures[tempIndex]);
        TideUp();
    }

    private void Next()
    {
        tempIndex--;
        tempIndex = tempIndex < 0 ? (mapCount - 1) : tempIndex;
        GenerateMap(mapTextures[tempIndex]);
        TideUp();
    }

    void GenerateMap(Texture2D texture)
    {
        ReclaimAllTiles();
        listTiles.Clear();

        float textureRatio = (float)texture.width / (float)texture.height;
        standardTextureSizeY = standardTextureSizeX / textureRatio;
        float ratioX = standardTextureSizeX / (float)texture.width;
        float ratioY = standardTextureSizeY / (float)texture.height;
        tileParent.localPosition = new Vector3(-(texture.width * ratioX) / 2f + 0.5f, -mapHeight + 0.1f, -(texture.height * ratioY) / 2f + 4f);
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                GenerateTile(texture, x, y, ratioX, ratioY, spaceBetweenTiles);
            }
        }

        totalTilesAmount = tilesActiveAmount;
        transformTiles = new Transform[tilesActiveAmount];
        posTiles = new Vector4[tilesActiveAmount];
        colorTiles = new Vector4[tilesActiveAmount];
        for (int i = 0; i < tilesActiveAmount; i++)
        {
            Tile tile = listTiles[i];
            transformTiles[i] = tile.transform;
            posTiles[i] = new Vector4(tile.transform.position.x, tile.transform.position.y, tile.transform.position.z, scale.x);
            colorTiles[i] = tile.currentColor;
        }
        instancedIndirect.UpdateBuffers();
    }

    void GenerateTile(Texture2D texture, int x, int y, float ratioX, float ratioY, float spaceBetweenTiles = 0)
    {
        Color pixelColor = texture.GetPixel(x, y);
        Vector3 pos = new Vector3((float)x * ratioX, 0f, (float)y * ratioY);
        scale = new Vector3(ratioX * (1f - spaceBetweenTiles), mapHeight, ratioY * (1f - spaceBetweenTiles));
        Tile instance;

        if (pixelColor.a == 0) // The pixel is transparrent. Let's ignore it!
        {
            return;
        }

        int lastIndex = poolTilesRecycle.Count - 1;
        if (lastIndex >= 0)
        {
            instance = poolTilesRecycle[lastIndex];
            poolTilesRecycle.RemoveAt(lastIndex);
            instance.gameObject.SetActive(true);
        }
        else
        {
            instance = Instantiate(tilePrefab);
            instance.transform.SetParent(tileParent, false);
        }

        instance.Initialize(tilesActiveAmount, pos, scale, pixelColor);

        instance.id = tilesActiveAmount;
        tilesActiveAmount++;
        listTiles.Add(instance);
    }

    public void UpdateTransformPosition()
    {
        for (int i = 0; i < tilesActiveAmount; i++)
        {
            Vector3 temp = transformTiles[i].position;
            posTiles[i] = new Vector4(temp.x, temp.y, temp.z, scale.x);
        }
        instancedIndirect.UpdateBuffers();
    }

    public void SetScale(int id)
    {
        Vector4 temp = posTiles[id];
        temp.w = 0f;
        posTiles[id] = temp;
        instancedIndirect.UpdateBuffers();
    }

    bool isUpdating = false;
    IEnumerator C_UpdateBuffers()
    {
        isUpdating = true;
        yield return new WaitForSeconds(Time.deltaTime * 2);
        instancedIndirect.UpdateBuffers();
        isUpdating = false;
    }

    public bool IsCleared()
    {
        return tilesActiveAmount == 0;
    }

    public void Reclaim(Tile tile)
    {
        poolTilesRecycle.Add(tile);
        tile.gameObject.SetActive(false);
        tilesActiveAmount--;
    }

    [NaughtyAttributes.Button]
    public void ReclaimAllTiles()
    {
        poolTilesRecycle.Clear();
        foreach (Transform tile in tileParent)
        {
            Reclaim(tile.GetComponent<Tile>());
        }
        tilesActiveAmount = 0;
    }

    [NaughtyAttributes.Button]
    public void TideUp()
    {
        StartCoroutine(C_Tide(0f, 1f));
        isTidedUp = true;
    }

    [NaughtyAttributes.Button]
    public void TideDown()
    {
        StartCoroutine(C_Tide(-mapHeight + 0.1f, 0.5f));
        isTidedUp = false;
    }

    IEnumerator C_Tide(float to, float duratiion = 0.2f)
    {
        aura.gameObject.SetActive(true);
        float t = 0f;
        var temp = tileParent.localPosition;
        var temp1 = aura.localScale;
        while (t < 1f)
        {
            t += Time.deltaTime / duratiion;
            temp.y = Mathf.Lerp(temp.y, to, Mathf.SmoothStep(0f, 1f, t));
            aura.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * 3f, Mathf.SmoothStep(0f, 1f, t));
            tileParent.localPosition = temp;
            UpdateTransformPosition();
            yield return null;
        }
        aura.gameObject.SetActive(false);
    }

    public void ExplodeNuclearBomb()
    {
        nuclearBomb.Explode();
    }

    public void ShowBonus()
    {
        bonusPrefab.gameObject.SetActive(true);
        bonusPrefab.PullDown();
    }

    public void HideBonus()
    {
        bonusPrefab.PullUp();
    }

    public void LoadMapOrder()
    {
        currentMapOrder = PlayerPrefs.GetInt(MAPORDER);
    }

    public void SaveMapOrder()
    {
        PlayerPrefs.SetInt(MAPORDER, currentMapOrder);
    }

    #endregion
}
