﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, Menu, InGame, EndGame, GameOver
}
public enum InGamePhase
{
    NULL,
    Clean,
    Clear,
    Bonus,
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    public float levelDuration = 90;
    public float bonusDuration = 10;
    #endregion

    #region Params
    private static GameManager instance;
    public GameState currentState;
    public InGamePhase currentInGamePhase;
    UIManager uIManager;
    public bool timeOut;
    private Coroutine countDownCoroutine;
    #endregion

    #region Properties
    public static GameManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    public event System.Action<InGamePhase> OnInGamePhaseChanged;
    #endregion

    #region Methods
    private void Awake()
    {
        instance = this;
        Application.targetFrameRate = 300;
    }

    private void OnEnable()
    {
        uIManager = UIManager.Instance;
        OnInGamePhaseChanged += OnInGamePhaseChangedHandler;
    }

    private void Start()
    {
        StartGame();
    }

    public void ChangeGameState(GameState state)
    {
        currentState = state;
    }

    public void ChangeInGamePhase(InGamePhase phase)
    {
        if (currentState == GameState.InGame)
        {
            currentInGamePhase = phase;
        }
        else
        {
            currentInGamePhase = InGamePhase.NULL;
        }
        OnInGamePhaseChanged(phase);
    }

    void OnInGamePhaseChangedHandler(InGamePhase phase)
    {
        switch (phase)
        {
            case InGamePhase.NULL:
                break;
            case InGamePhase.Clean:
                StartCoroutine(C_InGameClean());
                CountDown(levelDuration);
                break;
            case InGamePhase.Clear:
                StartCoroutine(C_InGameClear());
                CountDown(levelDuration);
                break;
            case InGamePhase.Bonus:
                StartCoroutine(C_InGameBonus());
                CountDown(bonusDuration);
                break;
            default:
                break;
        }
    }

    IEnumerator C_InGameClean()
    {
        uIManager.ShowCleanFX();
        yield return null;
    }

    IEnumerator C_InGameClear()
    {
        WeaponManager.Instance.Initialize();
        uIManager.ShowRevealedFX();
        WeaponManager.Instance.ReclaimAllWeapons();
        MapManager.Instance.ExplodeNuclearBomb();
        yield return new WaitForSeconds(1f);
        FOWManager.Instance.HideFOW();
        yield return new WaitForSeconds(0.5f);
        uIManager.ShowClearFX();
        yield return new WaitForSeconds(1f);
        MapManager.Instance.TideUp();

    }

    IEnumerator C_InGameBonus()
    {
        WeaponManager.Instance.ReclaimAllWeapons();
        WeaponManager.Instance.Initialize();
        uIManager.ShowBonusFX();
        yield return new WaitForSeconds(0.5f);
        MapManager.Instance.ShowBonus();
        yield return new WaitForSeconds(bonusDuration);
        MapManager.Instance.HideBonus();
        yield return new WaitForSeconds(0.5f);
        EndGame();
    }

    public void StartGame()
    {
        ChangeGameState(GameState.Menu);
        uIManager.ShowMenu();
        Launcher.Instance.Initialize();
        MapManager.Instance.Initialize();
    }

    [NaughtyAttributes.Button]
    public void PlayGame()
    {
        ChangeGameState(GameState.InGame);
        ReInit();
        uIManager.ShowInGame();
        ChangeInGamePhase(InGamePhase.Clean);
    }

    public void ReInit()
    {
        MapManager.Instance.Initialize();
        WeaponManager.Instance.Initialize();
        FOWManager.Instance.Initialize();
        Launcher.Instance.Initialize();
        ScoreManager.Instance.Initialize();
        ItemManager.Instance.Initialize();
        uIManager.Initialize();
    }

    void CountDown(float duration)
    {
        if (countDownCoroutine != null)
        {
            StopCoroutine(countDownCoroutine);
        }
        countDownCoroutine = StartCoroutine(C_CountDown(duration));
    }

    IEnumerator C_CountDown(float duration)
    {
        timeOut = false;
        float t = duration;
        while (t > 0)
        {
            yield return new WaitForSeconds(1.0f);
            t--;
            uIManager.UpdateCountDown(t);
        }
        timeOut = true;
        if (!MapManager.Instance.IsCleared())
        {
            LoseGame();
        }
    }

    public void EndGame()
    {
        StartCoroutine(C_EndGame());
    }

    IEnumerator C_EndGame(float delayDuration = 1f)
    {
        yield return new WaitForSeconds(delayDuration);
        ChangeGameState(GameState.EndGame);
        uIManager.ShowEndGame();
        MapManager.Instance.currentMapOrder++;
        MapManager.Instance.SaveMapOrder();
        WeaponManager.Instance.ReclaimAllWeapons();
        ScoreManager.Instance.SaveScore();
    }

    public void LoseGame()
    {
        StartCoroutine(C_LoseGame());
    }

    IEnumerator C_LoseGame(float delayDuration = 1f)
    {
        yield return new WaitForSeconds(delayDuration);
        ChangeGameState(GameState.GameOver);
        uIManager.ShowGameOver();
        WeaponManager.Instance.ReclaimAllWeapons();
    }

    #endregion
}
