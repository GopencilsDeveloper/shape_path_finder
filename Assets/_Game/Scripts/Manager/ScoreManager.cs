﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    #region Const
    public static string SCORE = "Score";
    #endregion

    #region Editor Params
    public GameObject scoreParticlePrefab;
    public Transform scoreParticleParent;
    #endregion

    #region Params
    private static ScoreManager instance;
    List<GameObject> poolScoreParticleRecycle = new List<GameObject>();
    private bool isSpawning;
    public int currentScore;
    #endregion

    #region Properties
    public static ScoreManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
        LoadScore();
    }

    public void Initialize()
    {
        // currentScore = 0;
    }

    public void LoadScore()
    {
        currentScore = PlayerPrefs.GetInt(SCORE);
    }

    public void SaveScore()
    {
        PlayerPrefs.SetInt(SCORE, currentScore);
    }

    public void AddScore(int score)
    {
        currentScore += score;
    }

    public void DoScoreFX(float rate = 0.5f, int score = 1)
    {
        StartCoroutine(C_DoScoreFX(rate, score));
    }

    IEnumerator C_DoScoreFX(float rate = 0.5f, int score = 1)
    {
        if (!isSpawning)
        {
            GameObject instance = GetFromPool(score);
            RandomInterfaceScoreParticle(instance);
            yield return new WaitForSeconds(rate);
            Reclaim(instance);
        }
    }

    GameObject GetFromPool(int score = 1)
    {
        isSpawning = true;
        GameObject instance;
        int lastIndex = poolScoreParticleRecycle.Count - 1;
        if (lastIndex >= 0)
        {
            instance = poolScoreParticleRecycle[lastIndex];
            poolScoreParticleRecycle.RemoveAt(lastIndex);
            instance.gameObject.SetActive(true);
        }
        else
        {
            instance = Instantiate(scoreParticlePrefab);
            instance.transform.SetParent(scoreParticleParent, false);
        }
        instance.GetComponent<Text>().text = ("+" + score);
        AddScore(score);
        isSpawning = false;
        return instance;
    }

    void RandomInterfaceScoreParticle(GameObject scoreParticle)
    {
        scoreParticle.GetComponent<RectTransform>().localPosition = new Vector3(Random.Range(-50f, 50f), Random.Range(-15f, 15f), 0f);
        scoreParticle.GetComponent<Text>().fontSize = (int)Random.Range(5f, 25f);
    }

    void Reclaim(GameObject gameObjectToRecycle)
    {
        poolScoreParticleRecycle.Add(gameObjectToRecycle);
        gameObjectToRecycle.SetActive(false);
    }

    #endregion
}
