﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    private Renderer meshRenderer;
    private MaterialPropertyBlock propBlock;
    public int id;
    public Vector3 position;
    public Color currentColor;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        propBlock = new MaterialPropertyBlock();
        meshRenderer = GetComponent<Renderer>();
    }

    private void OnEnable()
    {
        transform.localRotation = Quaternion.identity;
    }

    public void Initialize(int id, Vector3 pos, Vector3 localScale, Color inputColor)
    {
        this.id = id;
        this.position = pos;
        this.currentColor = inputColor;

        SetPosition(position);
        SetLocalScale(localScale);
        // SetColor(currentColor);
    }

    public void SetPosition(Vector3 pos)
    {
        transform.localPosition = pos;
    }

    public void SetLocalScale(Vector3 localScale)
    {
        transform.localScale = localScale;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.Instance.currentInGamePhase == InGamePhase.Clear)
        {
            if (other.CompareTag("Bullet") || other.CompareTag("Bomb") || other.CompareTag("Explosion"))
            {
                MapManager.Instance.SetScale(this.id);
                MapManager.Instance.Reclaim(this);
            }

            if (other.CompareTag("Wall"))
            {
                print("Wall");
                MapManager.Instance.SetScale(this.id);
                MapManager.Instance.Reclaim(this);
            }
        }
    }

    #endregion
}
