﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Rendering;

public class Example : MonoBehaviour
{
    //Attach this script to the object you want to instance, such as a cube object.  It should have a mesh renderer on it.

    Mesh mesh;
    Material mat;

    //Make an empty game object and drag it into the obj variable in the editor.  This object's transform will be used as the transform for the instanced object.
    public GameObject obj;

    Matrix4x4[] matrix;

    public bool turnOnInstance = true;

    private void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        mat = GetComponent<Renderer>().material;
    }

    void InitData()
    {
        matrix = new Matrix4x4[10];
        Vector3 pos = new Vector3();
        Vector3 scale = Vector3.one * 0.5f;

        for (int i = 0; i < matrix.Length; i++)
        {
            pos = obj.transform.position + Vector3.one * i;
            matrix[i].SetTRS(pos, Quaternion.identity, scale);
        }
    }

    void Update()
    {
        if (turnOnInstance)
        {
            Draw();
        }
    }

    void Draw()
    {
        InitData();
        Graphics.DrawMeshInstanced(mesh, 0, mat, matrix, matrix.Length);
    }
}
