﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulb : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnCollisionEnter(Collision other)
    {
        TurnOff();
    }

    private void TurnOff()
    {

    }

    #endregion
}
