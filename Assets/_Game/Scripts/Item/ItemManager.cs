﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    #region Editor Params
    public GameObject[] itemSetPrefabs;
    #endregion

    #region Params
    private static ItemManager instance;

    #endregion

    #region Properties
    public static ItemManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods
    private void Awake()
    {
        instance = this;
    }

    public void Initialize()
    {
        if (GameManager.Instance.currentState == GameState.InGame && MapManager.Instance.currentMapOrder > 5)
        {
            RandomItemSet();
        }
    }

    public void RandomItemSet()
    {
        int rand = (int)Random.Range(0, itemSetPrefabs.Length);
        for (int i = 0; i < itemSetPrefabs.Length; i++)
        {
            if (i == rand)
            {
                itemSetPrefabs[i].SetActive(true);
            }
            else
            {
                itemSetPrefabs[i].SetActive(false);
            }
        }
    }

    #endregion
}
