﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    #region Editor Params
    public Transform coreObj;
    public float range;
    public float duration;
    public bool isStatic;
    public bool reverseDirection;
    #endregion

    #region Params
    private Vector3 tempPosCoreObj;
    private Vector3 pos1;
    private Vector3 pos2;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        tempPosCoreObj = coreObj.localPosition;

        pos1 = new Vector3(tempPosCoreObj.x - range / 2f, tempPosCoreObj.y, tempPosCoreObj.z);
        pos2 = new Vector3(tempPosCoreObj.x + range / 2f, tempPosCoreObj.y, tempPosCoreObj.z);

        if (reverseDirection)
        {
            SwapVector3(ref pos1, ref pos2);
        }
        Oscillate();
    }

    private void Update()
    {
    }

    [NaughtyAttributes.Button]
    public void Stop()
    {
        StopCoroutine(C_Oscillate());
    }

    [NaughtyAttributes.Button]
    public void Oscillate()
    {
        StartCoroutine(C_Oscillate());
    }

    IEnumerator C_Oscillate()
    {
        float t = 0;
        while (t <= 0f && !isStatic)
        {
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempPosCoreObj = Vector3.Lerp(pos1, pos2, Mathf.SmoothStep(0f, 1f, t));
                coreObj.localPosition = tempPosCoreObj;
                yield return null;
            }

            SwapVector3(ref pos1, ref pos2);
            t = 0f;
        }
    }

    private void SwapVector3(ref Vector3 a, ref Vector3 b)
    {
        Vector3 temp = a;
        a = b;
        b = temp;
    }

    private void OnTriggerEnter(Collider other)
    {
        print("trigger");
    }

    #endregion
}
