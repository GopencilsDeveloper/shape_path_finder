﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOWLarge : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    public SimpleFogOfWar.FogOfWarSystem fow;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods
    public void Generate()
    {
        fow.GenerateFOW();
    }
    #endregion
}
